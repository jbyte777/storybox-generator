This is an example of multi-agent application powered by PromptQL v4.x . It has a simple network of Client, Story Teller and Voice Actor agents. 

You can play with it to generate various short audio stories.
