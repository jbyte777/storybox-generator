package sweagent

import (
	routeragent "gitlab.com/jbyte777/storybox-generator/core/router-agent"
)

func New() *routeragent.TRouterAgent {
	return routeragent.New("Router")
}
