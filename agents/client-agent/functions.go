package clientagent

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"path"
)

var consoleReader = bufio.NewReader(os.Stdin)

func start(args []interface{}) interface{} {
	fmt.Println("This is an example of ML multiagent network for generating audio story. Type your idea below to get your audio story:")
	fmt.Print("> ")
	line, _ := consoleReader.ReadString('\n')
	if len(line) > 0 {
		line = line[:len(line) - 1]
	}

	return line
}

func showStory(args []interface{}) interface{} {
	if len(args) < 1 {
		return fmt.Errorf(
			"ClientAgent/showStory => not enough arguments",
		)
	}
	
	appReqs, isAppReqsStr := args[0].(string)
	if !isAppReqsStr {
		return fmt.Errorf(
			"ClientAgent/showStory => %v is not a string",
			args[0],
		)
	}

	pathToSaveFile := path.Join("data", "text-story.txt")
	err := ioutil.WriteFile(pathToSaveFile, []byte(appReqs), 0666)
	if err != nil {
		return fmt.Errorf(
			"ClientAgent/showStory => %v",
			err.Error(),
		)
	}

	fmt.Println("If you accept story, type \"$yes\", otherwise type your extras")
	fmt.Print("> ")
	line, _ := consoleReader.ReadString('\n')
	if len(line) > 0 {
		line = line[:len(line) - 1]
	}

	var response TShowStoryResponse
	if line == "$yes" {
		response.Action = "pass_story_to_actor"
	} else {
		response.Action = "generate_story"
		response.Extras = line
	}

	rawResponse, _ := json.Marshal(response)
	return string(rawResponse)
}

func extractExtras(args []interface{}) interface{} {
	if len(args) < 1 {
		return fmt.Errorf(
			"ClientAgent/extracExtras => not enough arguments",
		)
	}
	
	extras, isExtrasStr := args[0].(string)
	if !isExtrasStr {
		return fmt.Errorf(
			"ClientAgent/extractExtras => %v is not a string",
			args[0],
		)
	}

	var showStoryRes TShowStoryResponse
	err := json.Unmarshal([]byte(extras), &showStoryRes)
	if err != nil {
		return fmt.Errorf(
			"ClientAgent/extractExtras => %v",
			err.Error(),
		)
	}

	if len(showStoryRes.Extras) == 0 {
		return "_"
	}

	return showStoryRes.Extras
}

func doPass(args []interface{}) bool {
	if len(args) < 1 {
		return false
	}
	
	extras, isExtrasStr := args[0].(string)
	if !isExtrasStr {
		return false
	}

	var showStoryRes TShowStoryResponse
	err := json.Unmarshal([]byte(extras), &showStoryRes)
	if err != nil {
		return false
	}

	return showStoryRes.Action == "pass_story_to_actor"
}

func callBack(args []interface{}) interface{} {
	fmt.Println("If you have any suggestions, type extras for story rework")
	fmt.Print("> ")
	line, _ := consoleReader.ReadString('\n')
	if len(line) > 0 {
		line = line[:len(line) - 1]
	}

	return line
}
