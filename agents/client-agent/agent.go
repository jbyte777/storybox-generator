package clientagent

import (
	"path"
	execagent "gitlab.com/jbyte777/storybox-generator/core/executive-agent"
	pqlcore "gitlab.com/jbyte777/prompt-ql/v5/core"
	promptql "gitlab.com/jbyte777/prompt-ql/v5/interpreter"
)

func New() *execagent.TExecutiveAgent {
	preinitedInternalVars := pqlcore.TGlobalVariablesTable{
		"_start": start,
		"_show_story": showStory,
		"_extract_extras": extractExtras,
		"_do_pass": doPass,
		"_callback": callBack,
	}

	return execagent.New(
		"Client",
		promptql.PromptQLOptions{
			PreinitializedInternalGlobals: preinitedInternalVars,
		},
		path.Join(
			"agents", "client-agent", "definitions.pql",
		),
		true,
	)
}
