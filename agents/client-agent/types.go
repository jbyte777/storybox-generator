package clientagent

type TShowStoryResponse struct {
	Action string `json:"action"`
	Extras string `json:"extras"`
}
