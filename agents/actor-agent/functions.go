package actoragent

import (
	"fmt"
	"io/ioutil"
	"path"
)

func saveAudio(args []interface{}) interface{} {
	if len(args) < 1 {
		return fmt.Errorf(
			"OnyxActorAgent/saveAudio => not enough arguments",
		)
	}
	
	audio, isAudioBytes := args[0].([]byte)
	if !isAudioBytes {
		return fmt.Errorf(
			"OnyxActorAgent/saveAudio => %v is not a byte slice",
			args[0],
		)
	}

	pathToSaveFile := path.Join("data", "audio-story.mp3")
	err := ioutil.WriteFile(pathToSaveFile, []byte(audio), 0666)
	if err != nil {
		return fmt.Errorf(
			"OnyxActorAgent/saveAudio => %v",
			err.Error(),
		)
	}

	return nil
}
