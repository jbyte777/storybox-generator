package actoragent

import (
	"path"
	execagent "gitlab.com/jbyte777/storybox-generator/core/executive-agent"
	promptql "gitlab.com/jbyte777/prompt-ql/v5/interpreter"
	pqlcore "gitlab.com/jbyte777/prompt-ql/v5/core"
)

func New(openAiUrl string, openAiKey string, openAiTimeoutSec uint) *execagent.TExecutiveAgent {
	preinitedInternalVars := pqlcore.TGlobalVariablesTable{
		"_save_audio": saveAudio,
	}
	
	return execagent.New(
		"OnyxActor",
		promptql.PromptQLOptions{
			PreinitializedInternalGlobals: preinitedInternalVars,
			OpenAiKey: openAiKey,
			OpenAiBaseUrl: openAiUrl,
			OpenAiListenQueryTimeoutSec: openAiTimeoutSec,
		},
		path.Join(
			"agents", "actor-agent", "definitions.pql",
		),
		false,
	)
}
