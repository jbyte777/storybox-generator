package actoragent

import (
	"strings"
)

func isCharEscaped(ch rune) bool {
	return ch == '{' || ch == '}' ||
		ch == '<' || ch == '>' || ch == '%'
}

func escapeResult(str string) string {
	res := strings.Builder{}
	strRune := []rune(str)

	for _, ch := range strRune {
		if isCharEscaped(ch) {
			res.WriteString("\\\\\\")
		}
		res.WriteRune(ch)
	}

	return res.String()
}
