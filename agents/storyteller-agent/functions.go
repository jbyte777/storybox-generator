package storytelleragent

import (
	"fmt"
	"regexp"
)

func extractStory(args []interface{}) interface{} {
	if len(args) < 1 {
		return fmt.Errorf(
			"StoryTellerAgent/extractStory => not enough arguments",
		)
	}
	
	story, isStoryStr := args[0].(string)
	if !isStoryStr {
		return fmt.Errorf(
			"StoryTellerAgent/extractStory => %v is not a string",
			args[0],
		)
	}

	cleanStoryPattern := regexp.MustCompile(`\[BEGIN_STORY\](.*)\[END_STORY\]`)
	cleanStoryIds := cleanStoryPattern.FindStringSubmatchIndex(story)
	if len(cleanStoryIds) < 4 {
		return escapeResult(story)
	}

	return escapeResult(story[cleanStoryIds[2]:cleanStoryIds[3]])
}
