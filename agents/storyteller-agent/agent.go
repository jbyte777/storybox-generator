package storytelleragent

import (
	"path"
	execagent "gitlab.com/jbyte777/storybox-generator/core/executive-agent"
	pqlcore "gitlab.com/jbyte777/prompt-ql/v5/core"
	promptql "gitlab.com/jbyte777/prompt-ql/v5/interpreter"
)

func New(openAiUrl string, openAiKey string, openAiTimeoutSec uint) *execagent.TExecutiveAgent {
	preinitedInternalVars := pqlcore.TGlobalVariablesTable{
		"_extract_story": extractStory,
	}

	return execagent.New(
		"StoryTeller",
		promptql.PromptQLOptions{
			PreinitializedInternalGlobals: preinitedInternalVars,
			OpenAiKey: openAiKey,
			OpenAiBaseUrl: openAiUrl,
			OpenAiListenQueryTimeoutSec: openAiTimeoutSec,
		},
		path.Join(
			"agents", "storyteller-agent", "definitions.pql",
		),
		false,
	)
}
