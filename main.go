package main

import (
	"encoding/json"
	"io/ioutil"
	"sync"

	clientagentmod "gitlab.com/jbyte777/storybox-generator/agents/client-agent"
	storytelleragentmod "gitlab.com/jbyte777/storybox-generator/agents/storyteller-agent"
	actoragentmod "gitlab.com/jbyte777/storybox-generator/agents/actor-agent"
	routeragentmod "gitlab.com/jbyte777/storybox-generator/agents/router-agent"
)

type TConfig struct {
	OpenAiKey string `json:"openAiKey"`
}

func main() {
	rawConfig, err := ioutil.ReadFile("config.json")
	if err != nil {
		panic(err)
	}

	var config struct {
		OpenAiKey string `json:"openAiKey"`
	}
	err = json.Unmarshal(rawConfig, &config)
	if err != nil {
		panic(err)
	}

	clientAgent := clientagentmod.New()
	storyTellerAgent := storytelleragentmod.New(
		"",
		config.OpenAiKey,
		150,
	)
	actorAgent := actoragentmod.New(
		"",
		config.OpenAiKey,
		150,
	)
	router := routeragentmod.New()

	clientAgent.ConnectPeer(router)
	storyTellerAgent.ConnectPeer(router)
	actorAgent.ConnectPeer(router)

	router.ConnectPeer(clientAgent)
	router.ConnectPeer(storyTellerAgent)
	router.ConnectPeer(actorAgent)

	rootWg := sync.WaitGroup{}
	rootWg.Add(4)

	router.InitAgent(&rootWg)
	clientAgent.InitAgent(&rootWg)
	storyTellerAgent.InitAgent(&rootWg)
	actorAgent.InitAgent(&rootWg)

	clientAgent.GetCmdChan() <- `{~embed_exp name="start" /}`
	rootWg.Wait()
}
