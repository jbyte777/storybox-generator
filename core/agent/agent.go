package agent

import (
	"sync"
)

type TAgent interface {
	GetId() string
	GetCmdChan() (chan string)
	ConnectPeer(peer TAgent)
	IsAgentClosed() bool
	IsAgentInitiator() bool
	InitAgent(rootWg *sync.WaitGroup) error
}
