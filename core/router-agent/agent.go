package routeragent

import (
	"fmt"
	"sync"
	"time"

	agent "gitlab.com/jbyte777/storybox-generator/core/agent"
)

type TRouterAgent struct {
	id string
	ownerChan           chan string
	isOwnerClosed       bool
	peersTable map[string]agent.TAgent
	initiatorPeer agent.TAgent
}

func New(
	id string,
) *TRouterAgent {
	return &TRouterAgent{
		id: id,
		ownerChan:           make(chan string),
		isOwnerClosed:       true,
		peersTable: make(map[string]agent.TAgent),
		initiatorPeer: nil,
	}
}

func (self *TRouterAgent) GetId() string {
	return self.id
}

func (self *TRouterAgent) GetCmdChan() chan string {
	return self.ownerChan
}

func (self *TRouterAgent) ConnectPeer(peer agent.TAgent) {
	self.peersTable[peer.GetId()] = peer

	if peer.IsAgentInitiator() {
		self.initiatorPeer = peer
	}
}

func (self *TRouterAgent) IsAgentClosed() bool {
	return self.isOwnerClosed
}

func (self *TRouterAgent) IsAgentInitiator() bool {
	return false
}

func (self *TRouterAgent) InitAgent(rootWg *sync.WaitGroup) error {
	// Initiative agent goroutine
	go func(self *TRouterAgent) {
		for msg := range self.ownerChan {
			time.Sleep(time.Millisecond * 300)

			if msg == "[MSG_RESTART_CHAIN]" {
				self.initiatorPeer.GetCmdChan() <- `{~embed_exp name="start" /}`
				continue
			}

			// determine peer
			msgInfo, err := parseMessage(msg)
			if err != nil {
				fmt.Printf(
					"[ERROR] Agent: %v / message loop => %v\n",
					self.id,
					err.Error(),
				)
				self.initiatorPeer.GetCmdChan() <- `{~embed_exp name="start" /}`
				continue
			}

			peer, hasPeer := self.peersTable[msgInfo.ToAgent]
			if !hasPeer {
				fmt.Printf(
					"[ERROR] Agent: %v / message loop => agent by id \"%v\" does not exist\n",
					self.id,
					msgInfo.ToAgent,
				)
				self.initiatorPeer.GetCmdChan() <- `{~embed_exp name="start" /}`
				continue
			}

			peer.GetCmdChan() <- msgInfo.Body
		}

		rootWg.Done()
	}(self)

	return nil
}
