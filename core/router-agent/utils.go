package routeragent

import (
	"encoding/json"
	"fmt"
	"regexp"
)

type THeader struct {
	FromAgent string `json:"fromAgent"`
	ToAgent string `json:"toAgent"`
}

type TMessage struct {
	FromAgent string
	ToAgent string
	Body string
}

func parseMessage(msg string) (*TMessage, error) {
	reg := regexp.MustCompile(`HEADER:(.*)\n===\n(.*)`)
	match := reg.FindStringSubmatchIndex(msg)
	if match == nil || len(match) < 6 {
		return nil, fmt.Errorf(
			"no header or body specified in \"%v\"",
			msg,
		)
	}

	headerJsonStr := msg[match[2]:match[3]]
	var msgHeader THeader
	err := json.Unmarshal([]byte(headerJsonStr), &msgHeader)
	if err != nil {
		return nil, err
	}

	return &TMessage{
		FromAgent: msgHeader.FromAgent,
		ToAgent: msgHeader.ToAgent,
		Body: msg[match[4]:],
	}, nil
}
