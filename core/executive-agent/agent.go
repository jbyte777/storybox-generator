package executiveagent

import (
	"fmt"
	"io/ioutil"
	"sync"
	"time"

	agent "gitlab.com/jbyte777/storybox-generator/core/agent"
	interpretercore "gitlab.com/jbyte777/prompt-ql/v5/core"
	pqlcore "gitlab.com/jbyte777/prompt-ql/v5/core"
	promptql "gitlab.com/jbyte777/prompt-ql/v5/interpreter"
)

type TExecutiveAgent struct {
	id string
	interpreter         *promptql.PromptQL
	ownerChan           chan string
	isOwnerClosed       bool
	peer                agent.TAgent
	definitionsFilePath string
	isAgentInitiator bool
}

func New(
	id string,
	interpreterOpts promptql.PromptQLOptions,
	definitionsFilePath string,
	isAgentInitiator bool,
) *TExecutiveAgent {
	interpreterOpts.RestrictedCommands = pqlcore.TRestrictedCommands{
		"unsafe_clear_stack": true,
		"unsafe_clear_vars": true,
		"unsafe_preinit_vars": true,
		"embed_def": true,
		"session_begin": true,
		"session_end": true,
	}

	interpreter := promptql.New(interpreterOpts)

	return &TExecutiveAgent{
		id: id,
		interpreter:         interpreter,
		ownerChan:           make(chan string),
		isOwnerClosed:       true,
		peer:                nil,
		definitionsFilePath: definitionsFilePath,
		isAgentInitiator: isAgentInitiator,
	}
}

func (self *TExecutiveAgent) GetId() string {
	return self.id
}

func (self *TExecutiveAgent) GetCmdChan() chan string {
	return self.ownerChan
}

func (self *TExecutiveAgent) ConnectPeer(peer agent.TAgent) {
	self.peer = peer
	self.isOwnerClosed = false
}

func (self *TExecutiveAgent) IsAgentClosed() bool {
	return self.isOwnerClosed
}

func (self *TExecutiveAgent) IsAgentInitiator() bool {
	return self.isAgentInitiator
}

func (self *TExecutiveAgent) restartChain() {
	fullInstance := self.interpreter.Instance.(*interpretercore.Interpreter)
	fullInstance.ControlFlowClearStack()

	restartSig, _ := self.interpreter.Instance.UnsafeExecute(`
		{~msg_restart_chain /}
		{~unsafe_clear_vars /}
		{~unsafe_clear_stack /}
	`).ResultDataStr()
	self.peer.GetCmdChan() <- restartSig
}

func (self *TExecutiveAgent) InitAgent(rootWg *sync.WaitGroup) error {
	definitions, err := ioutil.ReadFile(self.definitionsFilePath)
	if err != nil {
		return fmt.Errorf(
			"[ERROR] ExecutiveAgent / InitAgent => %v\n",
			err.Error(),
		)
	}

	initResult := self.interpreter.Instance.UnsafeExecute(
		fmt.Sprintf(
			`
			{~session_begin /}
			%v
			{~unsafe_clear_vars /}
			{~unsafe_clear_stack /}
			`,
			string(definitions),
		),
	)
	if initResult.Error != nil {
		return fmt.Errorf(
			"[ERROR] ExecutiveAgent / InitAgent => %v\n",
			err.Error(),
		)
	}

	// Initiative agent goroutine
	go func(self *TExecutiveAgent) {
		interpreter := self.interpreter.Instance
		peerChan := self.peer.GetCmdChan()

		for cmd := range self.ownerChan {
			time.Sleep(time.Millisecond * 300)

			// expansion phase
			expansionRes := interpreter.Execute(cmd)
			if expansionRes.Error != nil {
				fmt.Printf(
					"[ERROR] Agent: %v / message loop => %v\n",
					self.id,
					expansionRes.Error.Error(),
				)
				self.restartChain()
				continue
			}
			resultOnExpStr, isResultExpStr := expansionRes.ResultDataStr()
			errorOnExpStr, isErrorExpStr := expansionRes.ResultErrorStr()
			interpreter.UnsafeExecute(
				`
					{~unsafe_clear_vars /}
					{~unsafe_clear_stack /}
				`,
			)
			if !isResultExpStr {
				fmt.Printf(
					"[ERROR] Agent: %v / message loop => expansion \"%v\" is not a string\n",
					self.id,
					expansionRes,
				)
				self.restartChain()
				continue
			}
			if isErrorExpStr && len(errorOnExpStr) > 1 {
				fmt.Printf("[ERROR] Agent: %v / on code expansion => \n%v\n", self.id, cmd)
				fmt.Printf(
					"[ERROR] Agent: %v / message loop => %v\n",
					self.id,
					errorOnExpStr,
				)
				self.restartChain()
				continue
			}

			// execution phase
			interpreter.UnsafeExecute(`
				{~unsafe_preinit_vars /}
				{~unsafe_clear_stack /}
			`)
			executionRes := interpreter.Execute(resultOnExpStr)
			if executionRes.Error != nil {
				fmt.Printf(
					"[ERROR] Agent: %v / message loop => %v\n",
					self.id,
					executionRes.Error.Error(),
				)
				self.restartChain()
				continue
			}
			resultOnExecStr, isResultExecStr := executionRes.ResultDataStr()
			errorOnExecStr, isErrorExecStr := executionRes.ResultErrorStr()
			interpreter.UnsafeExecute(
				`
					{~unsafe_clear_vars /}
					{~unsafe_clear_stack /}
				`,
			)
			if !isResultExecStr {
				fmt.Printf(
					"[ERROR] Agent: %v / message loop => execution \"%v\" is not a string\n",
					self.id,
					executionRes,
				)
				self.restartChain()
				continue
			}
			if isErrorExecStr && len(errorOnExecStr) > 1 {
				fmt.Printf("[ERROR] Agent: %v / on code execution => \n%v\n", self.id, resultOnExpStr)
				fmt.Printf(
					"[ERROR] Agent: %v / message loop => %v\n",
					self.id,
					errorOnExecStr,
				)
				self.restartChain()
				continue
			}

			// send response back to peer
			if len(resultOnExecStr) > 0 {
				peerChan <- resultOnExecStr
			} else {
				self.restartChain()
			}
		}

		rootWg.Done()
	}(self)

	return nil
}
